import typeToReducer from "type-to-reducer";
import { actionTypes } from "../actions/championAction";

const { GETCHAMPOINBYYAER, LOADING } = actionTypes;

const INITIAL_STATE = {};

/**
 * instead of a switch case for handling diffrent types, we have an object to make it more readable and maintainable and then transfer to a reducer
 */
const reducers = {
  [GETCHAMPOINBYYAER]: (state, payload) => {
    return {
      ...state,
      [payload.year]: payload[payload.year],
      [`${payload.year}_loading`]: false
    };
  },
  [LOADING]: (state, payload) => {
    return {
      ...state,
      [`${payload.year}_loading`]: payload[`${payload.year}_loading`]
    };
  }
};

export default typeToReducer(reducers, INITIAL_STATE);
