import season from "./seasonReducer";
import champion from "./championReducer";

export default {
  season,
  champion
};
