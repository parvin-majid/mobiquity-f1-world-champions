import { actionTypes } from "../actions/seasonAction";
import typeToReducer from "type-to-reducer";

const { SETYEAR } = actionTypes;

const INITIAL_STATE = {
  year: undefined
};

/**
 * instead of a switch case for handling diffrent types, we have an object to make it more readable and maintainable and then transfer to a reducer
 */
const reducers = {
  [SETYEAR]: (state, payload) => {
    return {
      ...state,
      year: payload.year,
      worldChampionId: payload.worldChampionId
    };
  }
};

export default typeToReducer(reducers, INITIAL_STATE);
