import React from "react";
import { render } from "@testing-library/react";
import Header from "../components/Header";

describe("<Header /> Test", () => {
  it("Render Header with title prop must have h5 element", () => {
    const { getByText } = render(<Header title="it a test" />);

    const title = getByText("it a test");
    expect(title.nodeName).toBe("H5");
  });
});
