export default {
    seasonStart: process.env.SEASON_START || 2005, // start year of our range in Seasons component
    seasonEnd: process.env.SEASON_END || 2016, // end year of our range in Seasons component
    f1BaseUrl:  process.env.F1_BASE_URL || 'http://ergast.com/api/f1/' // base url of API for receving date related for f1 world champion
} 
