/**
 * creating specific action types for Champion
 * @param {string} prefix 
 * it is usually same as store name to make it most specefic
 * @param {Array} actionTypeArray 
 * passing all the action types needed for this 
 */
export const createActionTypeMap = (prefix, actionTypeArray) => {
    const actionTypeMap = {};
    for (const actionType of actionTypeArray) {
      actionTypeMap[actionType] = `${prefix}_${actionType}`;
    }
    return actionTypeMap;
  };
  