import axios from "axios";
import jsonpAdapter from 'axios-jsonp';
import config from "../configs/season";

/**
 * calling the API for receiving World Champion in year
 * @param {int} year
 * it is needed for calling the API to receive specefic data in the year
 */
export const getF1WorldChampionByYear = async year => {
  const response = await axios.get(
    `${config.f1BaseUrl}${year}/driverStandings/1.json`,
    {
      adapter: jsonpAdapter
    }
  );
  return response.data.MRData.StandingsTable.StandingsLists[0]
    .DriverStandings[0].Driver;
};

/**
 * calling the API for receiving all winners in year
 * @param {int} year
 * it is needed for calling the API to receive specefic data in the year
 */
export const getAllF1WinnersByYear = async year => {
  const response = await axios.get(
    `${config.f1BaseUrl}${year}/results/1.json`,
    {
      adapter: jsonpAdapter
    }
  );
  return response.data.MRData.RaceTable.Races;
};
