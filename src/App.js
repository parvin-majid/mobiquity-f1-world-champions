import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Routes from "./routes/router";

function App() {
  return (
    <div>
      <Header title="F1 World Champions" />
      <Routes />
      <Footer />
    </div>
  );
}

export default App;
