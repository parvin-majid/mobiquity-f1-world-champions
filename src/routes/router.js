import React from "react";
import { Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import Welcome from "../components/Welcome";
import Winners from "../components/winner/WinnersContainer";
/**
 * Router management for two different container screens
 */
export default () => (
  <BrowserRouter>
    <Route
      path="/seasons/:year"
      render={props => {
        return <Winners />;
      }}
    />
    <Route path="/" exact component={Welcome} />
  </BrowserRouter>
);
