import React from "react";

import { withStyles } from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import LinearProgress from "@material-ui/core/LinearProgress";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Link from "@material-ui/core/Link";
import Fab from "@material-ui/core/Fab";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import Seasons from "../season/SeasonsContainer";
import { getAllF1WinnersByYear } from "../../services/f1DataService";

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

const styles = theme => ({
  root: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: theme.palette.common.white,
    [theme.breakpoints.down("sm")]: {
      paddingLeft: 20,
      paddingRight: 20
    }
  },
  title: {
    color: theme.palette.primary.main,
    fontWeight: theme.typography.fontWeightLight,
    marginBottom: 10,
    [theme.breakpoints.down("sm")]: {
      fontSize: 30
    }
  },
  linearProgress: {
    flexGrow: 1
  },
  table: {
    width: "100%"
  },
  fab: {
    position: "fixed",
    top: 7,
    right: 10,
    backgroundColor: "#f38fb1"
  }
});

/**
 * Winner
 * @class Winner
 * @extends {Component}
 */
class Winner extends React.Component {

  // state for having list of winners in a specefic year and loading for showing progress bar
  state = {
    winners: [],
    loading: true
  };

  getWinners = async () => {
    // show loading icon and make list empty 
    this.setState({ winners: [], loading: true }); 

    // calling for receiving all winners in a year
    const winners = await getAllF1WinnersByYear(this.props.year); 

    // hide loading icon and fill the winners array 
    this.setState({ winners, loading: false });
  };

  async componentDidMount() {
    this.getWinners();
  }

  async componentDidUpdate(prevProps) {
    
    // update winners list in case of receving new year prop
    if (prevProps.year !== this.props.year) {
      this.getWinners();
    }
  }

  render() {
    const { classes, year, worldChampionId, history, setYear } = this.props;
    const { winners, loading } = this.state;
    return (
      <React.Fragment>
        <Fab
          size="medium"
          aria-label="back"
          className={classes.fab}
          onClick={() => {
            setYear("");
            history.push("/");
          }}
        >
          <ArrowBackIcon />
        </Fab>
        <Seasons />
        <Grid container justify={"center"} className={classes.root}>
          <Grid item xs={12} sm={12} md={9}>
            {!loading && (
              <Typography variant={"h5"} className={classes.title}>
                All Winners in {year}
              </Typography>
            )}
            {loading && (
              <div className={classes.linearProgress}>
                <LinearProgress />
              </div>
            )}
          </Grid>
          <Grid item xs={12} sm={12} md={9} container>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <StyledTableCell>Winner Name</StyledTableCell>
                  <StyledTableCell align="left">Date/Time</StyledTableCell>
                  <StyledTableCell align="left">Circuit Name</StyledTableCell>
                  <StyledTableCell align="left">Race name</StyledTableCell>
                  <StyledTableCell align="left">Round</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {loading && (
                  <StyledTableRow>
                    <StyledTableCell align="left" colSpan="10">
                      {"Loading..."}
                    </StyledTableCell>
                  </StyledTableRow>
                )}
                {winners.map(row => {
                  return (
                    <StyledTableRow
                      className={classes.tableRow}
                      key={row.round}
                      style={{
                        backgroundColor:
                          row.Results[0].Driver.driverId === worldChampionId
                            ? "rgb(192, 238, 255)"
                            : ""
                      }}
                    >
                      <StyledTableCell
                        component="th"
                        align="center"
                        scope="row"
                      >
                        {row.Results[0].Driver.givenName}{" "}
                        {row.Results[0].Driver.familyName}
                      </StyledTableCell>
                      <StyledTableCell align="left">
                        {row.date} {row.time.slice(0, -4)}
                      </StyledTableCell>
                      <StyledTableCell align="left">
                        <Link target="_blank" href={row.url}>
                          <span role="img" aria-label="">
                            &#128279;
                          </span>{" "}
                          {row.Circuit.circuitName}
                        </Link>
                      </StyledTableCell>
                      <StyledTableCell align="left">
                        {row.raceName}
                      </StyledTableCell>
                      <StyledTableCell align="left">
                        {row.round}
                      </StyledTableCell>
                    </StyledTableRow>
                  );
                })}
              </TableBody>
            </Table>

            {winners && !loading && winners.length === 0 && (
              <Typography variant={"h5"}>
                There is nothing to display
              </Typography>
            )}
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(Winner);
