import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { setYear } from "../../actions/seasonAction";
import Winners from "./Winners";

/**
 * connect the Winners component to redux store
 */
const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      setYear
    },
    dispatch
  )
});

const mapStatetoProps = ({ season: { year, worldChampionId } }) => {
  return {
    year,
    worldChampionId
  };
};

export default withRouter(
  connect(
    mapStatetoProps,
    mapDispatchToProps
  )(Winners)
);
