import React from "react";
import { withStyles } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";

const styles = {
  footer: {
    borderTop: "1px solid #e1e1e1",
    padding: "17px 0 15px",
    margin: "auto",
    color: "#888",
    fontSize: 12,

    "&>span": {
      marginRight: 16
    }
  }
};

/**
 * Footer of pages
 */
const Footer = props => {
  const { classes } = props;
  return (
    <Grid container justify={"center"}>
      <Grid item xs={12} sm={10} md={8}>
        <footer className={classes.footer} role="contentinfo">
          <span>Mobility 2019</span>
        </footer>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(Footer);
