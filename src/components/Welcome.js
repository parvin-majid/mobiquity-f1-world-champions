import React from "react";
import styled from "styled-components";
import Seasons from "./season/SeasonsContainer";

const Image = styled.img`
  width: 100%;
`;

/**
 * Content of landing page
 */
export default () => (
  <React.Fragment>
    <Seasons />
    <Image src="/images/RC118.png" />
  </React.Fragment>
);
