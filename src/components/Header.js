import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

/**
 * Header of pages
 * @props title
 * for showing the title of pages
 */
export default ({ title }) => {
  return (
    <AppBar position="static" color="primary">
      <Toolbar>
        <Typography variant="h5" color="textSecondary">
          {title}
        </Typography>
      </Toolbar>
    </AppBar>
  );
};
