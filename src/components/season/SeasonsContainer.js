import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { setYear } from "../../actions/seasonAction";
import { getChampionByYear } from "../../actions/championAction";
import Season from "./Seasons";

/**
 * connect the Seasons component to redux store
 */
const mapStatetoProps = ({ season: { year }, champion }) => {
  return {
    year,
    champion
  };
};

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(
    {
      setYear,
      getChampionByYear
    },
    dispatch
  )
});

export default withRouter(
  connect(
    mapStatetoProps,
    mapDispatchToProps
  )(Season)
);
