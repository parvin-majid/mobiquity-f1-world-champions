import React from "react";
import { withStyles } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import config from "../../configs/season";
import SeasonCard from "./card/SeasonCard";

const styles = theme => ({
  root: {
    paddingTop: 10,
    paddingBottom: 10,
    [theme.breakpoints.down("sm")]: {
      paddingLeft: 20,
      paddingRight: 20
    },
    cursor: "pointer"
  },
  title: {
    color: theme.palette.primary.main,
    fontWeight: theme.typography.fontWeightLight,

    [theme.breakpoints.down("sm")]: {
      fontSize: 40
    }
  },
  name: {
    fontWeight: theme.typography.fontWeightMedium
  },
  button: {
    margin: theme.spacing(2)
  },
  card: {
    width: "16%",
    float: "left",
    margin: "2px 3px",
    "&$selected, &$selected:hover": {
      backgroundColor: "#00AFEF"
    }
  },
  selected: {}
});

/**
 * Seasons component for showing list of world champion
 * @class Seasons
 * @extends {Component}
 */
class Seasons extends React.Component {
  componentDidMount() {
    if (!this.props.year && this.props.match.params.year) {
      this.props.setYear(this.props.match.params.year);
    }
  }

  render() {
    const {
      classes,
      setYear,
      year: selectedYear,
      history,
      getChampionByYear,
      champion
    } = this.props;
    return (
      <div className={classes.root}>
        <Grid container justify={"center"}>
          <Grid item xs={12} sm={10} md={8}>
            {Array.from(
              { length: config.seasonEnd - config.seasonStart + 1 },
              (_, i) => i + config.seasonStart
            ).map(year => (
              <SeasonCard
                key={year}
                year={year}
                className={classes.card}
                champion={champion}
                setYear={(year, worldChampionId) => {
                  setYear(year, worldChampionId);
                }}
                getChampionByYear={getChampionByYear}
                selected={year === selectedYear}
                history={history}
              />
            ))}
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Seasons);
