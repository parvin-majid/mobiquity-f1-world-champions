import React from "react";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActionArea from "@material-ui/core/CardActionArea";
import Card from "@material-ui/core/Card";
import CircularProgress from "@material-ui/core/CircularProgress";
import Link from "@material-ui/core/Link";

/**
 * SeasonCard for showing inside of Seasons component
 * @class SeasonCard
 * @extends {Component}
 */
class SeasonCard extends React.Component {
  async componentDidMount() {
    this.props.getChampionByYear(this.props.year);
  }
  render() {
    const {
      year,
      className,
      setYear,
      selected,
      history,
      champion
    } = this.props;
    const winner = champion[year];
    const loading = champion[`${year}_loading`];

    return (
      <Card
        className={className}
        style={{ backgroundColor: selected ? "rgb(192, 238, 255)" : "" }}
        onClick={() => {
          if (winner && winner.driverId) {
            setYear(year, winner.driverId);
            history.push(`/seasons/${year}`);
          }
        }}
      >
        <CardActionArea>
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {year}
            </Typography>
            {winner && (
              <Link href={winner.url} target="_blank">
                <span aria-label="" role="img">
                  &#127942;
                </span>
                {winner.givenName} {winner.familyName}
              </Link>
            )}
            {loading && <CircularProgress size={18} thickness={2} />}
          </CardContent>
        </CardActionArea>
      </Card>
    );
  }
}

export default SeasonCard;
