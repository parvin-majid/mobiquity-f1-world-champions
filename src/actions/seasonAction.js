import { createActionTypeMap } from "../utills";
import { getF1WorldChampionByYear } from "../services/f1DataService";

/**
 * creating specific action types for season
 * @ * it is usually same as store name to make it most specefic
 * @param {Array} actionTypeArray 
 * passing all the action types needed for this 
 */
export const actionTypes = createActionTypeMap("SEASON", ["SETYEAR"]);

const { SETYEAR } = actionTypes;

/**
 * calling the API for setting receiving year and World Champion Id in a year
 * @param {int} year
 * it is needed for calling the API to receive specefic data in the year
 * @param {string} worldChampionId
 * it is needed for storing in redux store for  highlighting in some places
 */
export const setYear = (year, worldChampionId) => async dispatch => {
  if (!worldChampionId && year) { // receiving related data if store is empty in case of reloading the page
    const response = await getF1WorldChampionByYear(year);
    worldChampionId = response.driverId;
  }

  // set year and worldChampionId in store
  return dispatch({
    type: SETYEAR,
    year: parseInt(year),
    worldChampionId
  });
};
