import { createActionTypeMap } from "../utills";
import { getF1WorldChampionByYear } from "../services/f1DataService";

/**
 * creating specific action types for Champion
 * @param {string} prefix 
 * it is usually same as store name to make it most specefic
 * @param {Array} actionTypeArray 
 * passing all the action types needed for this 
 */
export const actionTypes = createActionTypeMap("CHAMPOIN", [
  "GETCHAMPOINBYYAER",
  "LOADING"
]);

const { GETCHAMPOINBYYAER, LOADING } = actionTypes;
/**
 * calling the API for receiving Champion in a year
 * @param {int} year
 * it is needed for calling the API to receive specefic data in the year
 */
export const getChampionByYear = year => async (dispatch, getState) => {
  const yearChampoin = getState().champion[year];
  if (!yearChampoin) { 
    // check the store if this year data is not already received

    // dispatching loading for a specefic year
    dispatch({ type: LOADING, year, [`${year}_loading`]: true }); 

    const response = await getF1WorldChampionByYear(year);

    // dispatching received champion data for a specefic year 
    return dispatch({
      type: GETCHAMPOINBYYAER,
      [year]: response,
      year,
      [`${year}_loading`]: false
    });
  }
};
