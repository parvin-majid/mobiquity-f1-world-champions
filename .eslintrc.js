module.exports = {
  extends: ["react-app"],
  rules: {
    "import/no-cycle": "warn",
    "no-undef": "error",
    "import/no-extraneous-dependencies": "warn"
  }
};
