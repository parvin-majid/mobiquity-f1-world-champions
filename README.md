# This project is just a coding challenge for a Senior Front-End Develper role from Mobiquity Inc.

## Available Scripts

In the project directory, you can run:

#### `yarn install`

It is used to install all dependencies for a project.<br />
**It is necessary to run before staring the app**.

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3007](http://localhost:3007) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn doc`

Generate new documentation files based on esdoc.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

### Run a React App in a Docker Container

To build the image, you can run the following command from the project root folder, where the Dockerfile is:

`docker build -t react-mobiquity-app .`

Note that we used port 80 in the serve command, so need to make sure to use 80 when specifying the container port like this:

`docker run -it -p 8080:80 react-mobiquity-app`

Once the container is running, you can open http://localhost:8080 and you'll be able to access the React app running inside the Docker container.

## Web framework

The **React** has been used as a JavaScript library for building user interfaces.

## State Management

The **Redux** has been used in this project as state container.

## Design System

**MATERIAL-UI** has been used in this project for having Material Design.
